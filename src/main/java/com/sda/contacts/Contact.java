package com.sda.contacts;

import java.io.Serializable;

public class Contact implements Serializable {
    private String fullName;
    private String email;
    private String phoneNr;
    private String imageUUID;

    public Contact() {

    }

    public Contact(String fullName, String email, String phoneNr) {
        this.fullName = fullName;
        this.email = email;
        this.phoneNr = phoneNr;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String getImageUUID() {
        return imageUUID;
    }

    public void setImageUUID(String imageUUID) {
        this.imageUUID = imageUUID;
    }
}
