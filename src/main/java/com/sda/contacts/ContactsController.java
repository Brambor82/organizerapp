package com.sda.contacts;

import com.sda.files.ContactManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

public class ContactsController implements Initializable {
    private Map<String, Contact> contacts;
    private ContactManager cm = new ContactManager();

    @FXML
    Parent contactsPane;
    @FXML
    FlowPane contactsFlowPane;
    @FXML
    ScrollPane contactsScrollPane;

    @FXML
    TextField newNameTextField;
    @FXML
    TextField newEmailTextField;
    @FXML
    TextField newPhoneTextField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        contactsScrollPane.prefWidthProperty().bind(((Pane) contactsPane).widthProperty());
        contactsScrollPane.prefHeightProperty().bind(((Pane) contactsPane).heightProperty());

        //load from file
        contacts = cm.loadContacts();
        contacts.values().forEach(this::createPaneForContact);
    }

    private void createPaneForContact(Contact c) {
        Pane cPane = new Pane();
        Pane cTopPane = new Pane();
        cTopPane.getStyleClass().addAll("list-data-toppane", "contact-toppane");
        cPane.getStyleClass().addAll("list-data-pane", "contact-pane");

        ImageView contactImageView = new ImageView();
        Image image = null;
        if(c.getImageUUID()!=null){
            image = cm.loadContactImage(c.getImageUUID());
        }
        if (c.getImageUUID()==null || image == null){
            contactImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/contact.png")));
        }
        else {
            contactImageView.setImage(image);
        }
        contactImageView.setFitWidth(150);
        contactImageView.setFitHeight(150);
        contactImageView.setLayoutX(30);
        contactImageView.setY(100);
        //image circular clip
        final Circle clip = new Circle(75, 175, 75);
        contactImageView.setClip(clip);
        TextField cName = new TextField(c.getFullName());
        cName.getStyleClass().addAll("contact-name", "contact-field");
        cName.setEditable(false);
        ImageView deleteImageView = new ImageView();
        deleteImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/delete.png")));
        deleteImageView.setFitWidth(30);
        deleteImageView.setFitHeight(30);
        deleteImageView.setLayoutY(20);
        deleteImageView.setLayoutX(550);
        deleteImageView.getStyleClass().addAll("list-data-icon");
        ImageView editImageView = new ImageView();
        editImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/edit.png")));
        editImageView.getStyleClass().addAll("list-data-icon");
        editImageView.setFitWidth(30);
        editImageView.setFitHeight(30);
        editImageView.setLayoutY(20);
        editImageView.setLayoutX(480);
        ImageView okImageView = new ImageView();
        okImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/ok.png")));
        okImageView.getStyleClass().addAll("list-data-icon");
        okImageView.setVisible(false);
        okImageView.setFitWidth(30);
        okImageView.setFitHeight(30);
        okImageView.setLayoutY(20);
        okImageView.setLayoutX(400);

        cTopPane.getChildren().addAll(cName, deleteImageView, editImageView, okImageView);

        TextField cEmail = new TextField(c.getEmail());
        cEmail.getStyleClass().addAll("contact-email", "contact-field");
        cEmail.setEditable(false);
        cEmail.setLayoutX(180);
        cEmail.setLayoutY(100);
        TextField cPhoneNr = new TextField(c.getPhoneNr());
        cPhoneNr.getStyleClass().addAll("contact-phone", "contact-field");
        cPhoneNr.setEditable(false);
        cPhoneNr.setLayoutX(180);
        cPhoneNr.setLayoutY(160);

        cPane.getChildren().addAll(cTopPane, contactImageView, cEmail, cPhoneNr);

        deleteImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                contacts.remove(c.getFullName());
                contactsFlowPane.getChildren().remove(cPane);
                //update file
                cm.updateContacts(contacts);
            }
        });
        editImageView.setOnMouseClicked(event -> {
            cEmail.setEditable(true);
            cPhoneNr.setEditable(true);
            okImageView.setVisible(true);
            contactImageView.setCursor(Cursor.HAND);
            contactImageView.setOnMouseClicked(imageEvent -> {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Choose image for this contact");
                fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("JPG", "*.jpg")//,
                        /*new FileChooser.ExtensionFilter("PNG", "*.png")*/
                );
                File file = fileChooser.showOpenDialog(contactsPane.getScene().getWindow());
                if (file != null) {
                    contactImageView.setImage(new Image(file.toURI().toString()));
                }
            });

        });
        okImageView.setOnMouseClicked(event -> {
            okImageView.setVisible(false);
            cEmail.setEditable(false);
            cPhoneNr.setEditable(false);
            contactImageView.setCursor(Cursor.DEFAULT);
            contactImageView.setOnMouseClicked(null);
            c.setEmail(cEmail.getText());
            c.setPhoneNr(cPhoneNr.getText());
            //update image
            String imageUUID = UUID.randomUUID().toString();
            if(cm.saveContactImage(contactImageView.getImage(), imageUUID)){
                c.setImageUUID(imageUUID);
            }
            //update file
            cm.updateContacts(contacts);
        });
        contactsFlowPane.getChildren().add(cPane);
    }

    public void addContact(MouseEvent mouseEvent) {
        if (!(newNameTextField.getText().isEmpty() || newEmailTextField.getText().isEmpty() || newPhoneTextField.getText().isEmpty())) {
            Contact contact = new Contact(newNameTextField.getText(), newEmailTextField.getText(), newPhoneTextField.getText());
            contacts.put(contact.getFullName(), contact);
            contactsFlowPane.getChildren().remove(2, contactsFlowPane.getChildren().size());
            contacts.values().forEach(this::createPaneForContact);
            newEmailTextField.setText("");
            newNameTextField.setText("");
            newPhoneTextField.setText("");
            //save to file
            cm.updateContacts(contacts);
        }
    }

    public void returnToMain(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) contactsPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
