package com.sda.files;

public class ConfigDTO {
    private String userName;
    private String localization;
    private Integer caloriesAmount;
    private String visibleNote;

    public ConfigDTO(String userName, String localization, Integer caloriesAmount) {
        this.userName = userName;
        this.localization = localization;
        this.caloriesAmount = caloriesAmount;
    }

    public ConfigDTO(String userName, String localization, Integer caloriesAmount, String visibleNote) {
        this(userName, localization, caloriesAmount);
        this.visibleNote = visibleNote;
    }

    public String getUserName() {
        return userName;
    }

    public String getLocalization() {
        return localization;
    }

    public Integer getCaloriesAmount() {
        return caloriesAmount;
    }

    public String getVisibleNote() {
        return visibleNote;
    }

    public void setVisibleNote(String visibleNote) {
        this.visibleNote = visibleNote;
    }
}
