package com.sda.files;

import com.sda.notes.Note;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class NoteManager {
    private static final String NOTES = "saves/notes.bin";
    private Logger logger = LogManager.getLogger(ContactManager.class);

    public List<Note> loadNotes() {
        List<Note> result = new LinkedList<>();
        try {
            FileInputStream fis = new FileInputStream(NOTES);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Note note = null;
            while ((note = (Note) ois.readObject()) != null) {
                result.add(note);
            }
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("Could not load notes.");
            e.printStackTrace();
        }
        return result;
    }

    public void updateNotes(List<Note> notes) {
        try {
            FileOutputStream fos = new FileOutputStream(NOTES);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Note note : notes) {
                oos.writeObject(note);
            }
            oos.close();
        } catch (IOException e) {
            logger.error("Could not update notes.");
            e.printStackTrace();
        }
    }
}
