package com.sda.files;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class PropertiesManager {
    private static final String CONFIG = "saves/config.properties";
    private static final String CALORIES = "saves/calories.properties";
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private Logger logger = LogManager.getLogger(PropertiesManager.class);

    public void updateConfigProperties(ConfigDTO configDTO){
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CONFIG);
            properties.load(fr);
            fr.close();
            FileWriter fw = new FileWriter(CONFIG);
            properties.setProperty(PropertyName.USERNAME.name(), configDTO.getUserName());
            properties.setProperty(PropertyName.LOCALIZATION.name(), configDTO.getLocalization());
            properties.setProperty(PropertyName.CALORIES_AMOUNT.name(), configDTO.getCaloriesAmount().toString());
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            logger.error("config.properties file not found.");
            e.printStackTrace();
        }
    }

    public ConfigDTO loadConfigProperties(){
        String username = null;
        String localization = null;
        Integer caloriesAmount = null;
        String visibleNote = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CONFIG);
            properties.load(fr);
            username = properties.getProperty(PropertyName.USERNAME.name());
            localization = properties.getProperty(PropertyName.LOCALIZATION.name());
            caloriesAmount = Integer.valueOf(properties.getProperty(PropertyName.CALORIES_AMOUNT.name()));
            visibleNote = properties.getProperty(PropertyName.VISIBLE_NOTE.name());
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ConfigDTO(username, localization, caloriesAmount, visibleNote);
    }

    public void updateCaloriesAbsorbedProperty(Integer caloriesAbsorbed){
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CALORIES);
            properties.load(fr);
            fr.close();
            FileWriter fw = new FileWriter(CALORIES);
            properties.setProperty(PropertyName.CALORIES_ABSORBED.name(), caloriesAbsorbed.toString());
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            logger.error("calories.properties file not found.");
            e.printStackTrace();
        }
    }

    public Integer loadCaloriesAbsorbedProperty(){
        Integer caloriesAbsorbed = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CALORIES);
            properties.load(fr);
            String caloriesAbsorbedVal = properties.getProperty(PropertyName.CALORIES_ABSORBED.name());
            if(caloriesAbsorbedVal!=null) {
                caloriesAbsorbed = Integer.valueOf(caloriesAbsorbedVal);
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return caloriesAbsorbed;
    }

    public LocalDate loadCaloriesCurrentDateProperty(){
        LocalDate date = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CALORIES);
            properties.load(fr);
            String dateStr = properties.getProperty(PropertyName.CALORIES_CURRENT_DATE.name());
            if(dateStr!=null) {
                date = LocalDate.parse(dateStr, dtf);
            }
            fr.close();
        } catch (IOException e) {
            logger.error("Error loading date.");
            e.printStackTrace();
        }
        logger.info("Loaded date " + date);
        return date;
    }

    public void updateCaloriesCurrentDateProperty(LocalDate date){
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CALORIES);
            properties.load(fr);
            fr.close();
            FileWriter fw = new FileWriter(CALORIES);
            properties.setProperty(PropertyName.CALORIES_CURRENT_DATE.name(), (date).format(dtf));
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            logger.error("calories.properties file not found.");
            e.printStackTrace();
        }
    }

    public String loadVisibleNoteProperty(){
        String visibleNote = null;
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CONFIG);
            properties.load(fr);
            visibleNote = properties.getProperty(PropertyName.VISIBLE_NOTE.name());
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return visibleNote;
    }

    public void updateVisibleNoteProperty(String visibleNote){
        Properties properties = new Properties();
        try {
            FileReader fr = new FileReader(CONFIG);
            properties.load(fr);
            fr.close();
            FileWriter fw = new FileWriter(CONFIG);
            properties.setProperty(PropertyName.VISIBLE_NOTE.name(), visibleNote);
            properties.store(fw, "");
            fw.close();
        } catch (IOException e) {
            logger.error("config.properties file not found.");
            e.printStackTrace();
        }
    }
}
