package com.sda.main;

import com.sda.files.ConfigDTO;
import com.sda.files.PropertiesManager;
import com.sda.weather.WeatherDTO;
import com.sda.weather.WeatherLoader;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Controller implements Initializable {
    private Logger logger = LogManager.getLogger(Controller.class);
    private PropertiesManager pm = new PropertiesManager();
    private Integer caloriesLimit;
    private LocalDate dayDate;
    private WeatherLoader weatherLoader = new WeatherLoader();
    private static ScheduledExecutorService weatherUpdater;
    private static ScheduledExecutorService caloriesUpdater;

    @FXML
    Parent mainPane;
    @FXML
    ScrollPane mainScrollPane;

    //welcome
    @FXML
    Label usernameLabel;

    //notes
    @FXML
    Label visibleNoteLabel;

    //calories
    @FXML
    PieChart caloriesPieChart;
    @FXML
    Label caloriesAbsorbedLabel;
    @FXML
    TextField newCaloriesTextField;

    //weather
    @FXML
    Label wLocalizationLabel;
    @FXML
    Label wTheTempLabel;
    @FXML
    Label wMinTempLabel;
    @FXML
    Label wMaxTempLabel;
    @FXML
    ImageView wImageView;

    //events
    @FXML
    ListView<String> eventsTodayListView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadData();

        mainScrollPane.prefWidthProperty().bind(((Pane) mainPane).widthProperty());
        mainScrollPane.prefHeightProperty().bind(((Pane) mainPane).heightProperty());

        eventsTodayListView.getItems().add("Lukas's birthday.");
        eventsTodayListView.getItems().add("Annual meeting.");

        caloriesPieChart.setStartAngle(90);
    }

    public void loadSettings(MouseEvent mouseEvent) {
        stopExecutors();
        URL resource = getClass().getClassLoader().getResource("settings.fxml");
        if (resource != null) {
            try {
                Pane settingsPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(settingsPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadContacts(MouseEvent mouseEvent) {
        stopExecutors();
        URL resource = getClass().getClassLoader().getResource("contacts.fxml");
        if (resource != null) {
            try {
                Pane contactsPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(contactsPane, 700, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadNotes(MouseEvent mouseEvent) {
        stopExecutors();
        URL resource = getClass().getClassLoader().getResource("notes.fxml");
        if (resource != null) {
            try {
                Pane settingsPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(settingsPane, 700, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadData() {
        ConfigDTO configDTO = pm.loadConfigProperties();
        loadSceneLabels(configDTO);
        //calories
        this.dayDate = pm.loadCaloriesCurrentDateProperty();
        if(caloriesUpdater==null) {
            caloriesUpdater = Executors.newSingleThreadScheduledExecutor();
            caloriesUpdater.scheduleAtFixedRate(
                    () -> {
                        Platform.runLater(() -> {
                            //update calories when date changes
                            if (dayDate == null) {
                                dayDate = LocalDate.now();
                                pm.updateCaloriesCurrentDateProperty(dayDate);
                            } else if (!dayDate.equals(LocalDate.now())) {
                                dayDate = LocalDate.now();
                                updateCalories(0);
                                pm.updateCaloriesCurrentDateProperty(dayDate);
                                pm.updateCaloriesAbsorbedProperty(0);
                            }
                        });
                    }, 0, 1, TimeUnit.MINUTES);
        }
        Integer caloriesAbsorbed = pm.loadCaloriesAbsorbedProperty();
        if (caloriesAbsorbed == null) {
            caloriesAbsorbed = 0;
            pm.updateCaloriesAbsorbedProperty(0);
        }
        this.caloriesLimit = configDTO.getCaloriesAmount();
        updateCalories(caloriesAbsorbed);
        //weather
        updateWeatherInfo();
        if(weatherUpdater==null) {
            weatherUpdater = Executors.newSingleThreadScheduledExecutor();
            weatherUpdater.scheduleAtFixedRate(() -> {
                Platform.runLater(() -> {
                    updateWeatherInfo();
                    logger.info("Updated weather.");
                });
            }, 0, 30, TimeUnit.MINUTES);
        }
    }

    private void loadSceneLabels(ConfigDTO configDTO) {
        //welcome
        this.usernameLabel.setText(configDTO.getUserName() + "!");
        this.wLocalizationLabel.setText(configDTO.getLocalization());
        this.visibleNoteLabel.setText(configDTO.getVisibleNote() != null ? configDTO.getVisibleNote() : "no note selected");
    }

    public void addCalories(MouseEvent mouseEvent) {
        String newCaloriesInput = this.newCaloriesTextField.getText();
        try {
            Integer newCalories = Integer.valueOf(newCaloriesInput);
            Integer newCaloriesAbsorbed = Integer.valueOf(this.caloriesAbsorbedLabel.getText()) + newCalories;
            pm.updateCaloriesAbsorbedProperty(newCaloriesAbsorbed);
            //calories over limit
            if (newCaloriesAbsorbed > caloriesLimit) {
                updateCalories(caloriesLimit);
                caloriesAbsorbedLabel.setText(newCaloriesAbsorbed.toString());
            } else {
                updateCalories(newCaloriesAbsorbed);
            }
        } catch (NumberFormatException e) {
            logger.error("Wrong number format.");
            e.printStackTrace();
        }
        this.newCaloriesTextField.setText("0");
    }

    private void updateCalories(Integer absorbed) {
        this.caloriesAbsorbedLabel.setText(absorbed.toString());
        ObservableList<PieChart.Data> caloriesData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Absorbed", absorbed),
                        new PieChart.Data("Pending", caloriesLimit - absorbed));
        this.caloriesPieChart.setData(caloriesData);
    }

    private void updateWeatherInfo() {
        WeatherDTO weatherDTO = weatherLoader.loadWeather(this.wLocalizationLabel.getText().toLowerCase());
        if (weatherDTO != null) {
            this.wMaxTempLabel.setText((weatherDTO.getMaxTemp() != null ? weatherDTO.getMaxTemp().toString() : "-") + " C");
            this.wMinTempLabel.setText((weatherDTO.getMinTemp() != null ? weatherDTO.getMinTemp().toString() : "-") + " C");
            this.wTheTempLabel.setText((weatherDTO.getTheTemp() != null ? weatherDTO.getTheTemp().toString() : "-") + " C");
            this.wImageView.setImage(new Image("icons/weather-" + weatherDTO.getWeatherState().name().toLowerCase() + ".png"));
        }
    }

    public static void stopExecutors() {
        caloriesUpdater.shutdownNow();
        weatherUpdater.shutdownNow();
    }
}

