package com.sda.notes;

import com.sda.files.NoteManager;
import com.sda.files.PropertiesManager;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class NotesController implements Initializable {
    private List<Note> notes;
    private NoteManager nm = new NoteManager();
    private PropertiesManager pm = new PropertiesManager();
    private List<ToggleButton> buttons = new LinkedList<>();
    String visibleNote = null;

    @FXML
    Parent notesPane;
    @FXML
    FlowPane notesFlowPane;
    @FXML
    ScrollPane notesScrollPane;

    @FXML
    TextArea textTextArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        notesScrollPane.prefWidthProperty().bind(((Pane) notesPane).widthProperty());
        notesScrollPane.prefHeightProperty().bind(((Pane) notesPane).heightProperty());
        notesFlowPane.setOpaqueInsets(new Insets(30.0, 30.0, 30.0, 30.0));
        notesFlowPane.setHgap(30.0);
        notesFlowPane.setVgap(30.0);
        textTextArea.setTextFormatter(new TextFormatter<String>(change ->
                change.getControlNewText().length() <= 300 ? change : null));

        //load from file
        notes = nm.loadNotes();
        notes.forEach(this::createPaneForNote);
        //load visible note
        visibleNote = pm.loadVisibleNoteProperty();
        markVisibleNote();
    }

    private void markVisibleNote() {
        if (visibleNote == null) {
            buttons.get(0).setSelected(true);
        } else {
            Optional<ToggleButton> selected = buttons
                    .stream()
                    .filter(b -> (((TextArea) ((Pane) b.getParent().getParent()).getChildren().get(1)).getText().equals(visibleNote)))
                    .findFirst();
            if (selected.isPresent()) {
                selected.get().setSelected(true);
            }
        }
    }

    private void createPaneForNote(Note note) {
        Pane cPane = new Pane();
        Pane cTopPane = new Pane();
        cTopPane.getStyleClass().addAll("list-data-toppane", "note-toppane");
        cPane.getStyleClass().addAll("list-data-pane", "note-pane");

        ImageView deleteImageView = new ImageView();
        deleteImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/delete.png")));
        deleteImageView.getStyleClass().addAll("list-data-icon");
        deleteImageView.setFitWidth(30);
        deleteImageView.setFitHeight(30);
        deleteImageView.setLayoutY(20);
        deleteImageView.setLayoutX(550);
        ImageView editImageView = new ImageView();
        editImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/edit.png")));
        editImageView.getStyleClass().addAll("list-data-icon");
        editImageView.setFitWidth(30);
        editImageView.setFitHeight(30);
        editImageView.setLayoutY(20);
        editImageView.setLayoutX(480);
        ImageView okImageView = new ImageView();
        okImageView.setImage(new Image(getClass().getClassLoader().getResourceAsStream("icons/ok.png")));
        okImageView.getStyleClass().addAll("list-data-icon");
        okImageView.setVisible(false);
        okImageView.setFitWidth(30);
        okImageView.setFitHeight(30);
        okImageView.setLayoutY(20);
        okImageView.setLayoutX(400);
        ToggleButton visibleToggleButton = new ToggleButton("Main");
        visibleToggleButton.setSelected(false);
        visibleToggleButton.setLayoutX(0);
        visibleToggleButton.setLayoutY(0);
        visibleToggleButton.getStyleClass().add("note-toggle");
        buttons.add(visibleToggleButton);

        cTopPane.getChildren().addAll(visibleToggleButton, deleteImageView, editImageView, okImageView);
        TextArea nTextArea = new TextArea(note.getText());
        nTextArea.getStyleClass().add("note-area");
        nTextArea.setEditable(false);
        nTextArea.setLayoutX(0);
        nTextArea.setLayoutY(50);

        cPane.getChildren().addAll(cTopPane, nTextArea);
        notesFlowPane.getChildren().add(cPane);

        deleteImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                notes.remove(note);
                if (visibleToggleButton.isSelected()) {
                    visibleNote = null;
                    buttons.remove(visibleToggleButton);
                }
                notesFlowPane.getChildren().remove(cPane);
                //update file
                nm.updateNotes(notes);
            }
        });
        editImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                nTextArea.setEditable(true);
                okImageView.setVisible(true);
            }
        });
        okImageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                okImageView.setVisible(false);
                nTextArea.setEditable(false);
                note.setText(nTextArea.getText());
                //update file
                nm.updateNotes(notes);
            }
        });
        visibleToggleButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                visibleNote = nTextArea.getText();
                visibleToggleButton.setSelected(true);
                buttons.stream().filter(b -> b != visibleToggleButton).forEach(b -> b.setSelected(false));
            }
        });
    }

    public void addNote(MouseEvent mouseEvent) {
        if (!textTextArea.getText().isEmpty()) {
            Note note = new Note(textTextArea.getText());
            notes.add(note);
            notesFlowPane.getChildren().remove(2, notesFlowPane.getChildren().size());
            buttons = new LinkedList<>();
            notes.forEach(this::createPaneForNote);
            markVisibleNote();
            //save to file
            textTextArea.setText("");
            nm.updateNotes(notes);
        }
    }

    public void returnToMain(MouseEvent mouseEvent) {
        saveVisibleNote();
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) notesPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveVisibleNote() {
        Optional<ToggleButton> selected = buttons.stream().filter(ToggleButton::isSelected).findFirst();
        if(selected.isPresent()){
            ToggleButton button = selected.get();
            String note = ((TextArea)(((Pane)button.getParent().getParent()).getChildren().get(1))).getText();
            pm.updateVisibleNoteProperty(note);
        }
        pm.updateVisibleNoteProperty(visibleNote);
    }
}
