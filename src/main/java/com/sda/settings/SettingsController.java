package com.sda.settings;

import com.sda.files.ConfigDTO;
import com.sda.files.PropertiesManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    private Logger logger = LogManager.getLogger(SettingsController.class);
    private PropertiesManager pm;

    @FXML
    Parent settingsPane;

    @FXML
    Slider caloriesSlider;
    @FXML
    Label caloriesLabel;

    @FXML
    TextField usernameTextField;

    @FXML
    TextField localizationTextField;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle){
        pm = new PropertiesManager();
        loadSettings();
        this.caloriesLabel.setText(String.valueOf(Double.valueOf(this.caloriesSlider.getValue()).intValue()));
        this.caloriesSlider.valueProperty().addListener(
                (observable, oldValue, newValue) ->
                        caloriesLabel.setText(String.valueOf(newValue.intValue())));
    }

    public void returnToMain(MouseEvent mouseEvent) {
        saveSettings();
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if(resource!=null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) settingsPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveSettings(){
        String userName = this.usernameTextField.getText();
        String localization = this.localizationTextField.getText();
        Integer caloriesAmount = Double.valueOf(this.caloriesSlider.getValue()).intValue();
        ConfigDTO configDTO = new ConfigDTO(userName, localization, caloriesAmount);
        pm.updateConfigProperties(configDTO);
    }

    private void loadSettings(){
        ConfigDTO configDTO = pm.loadConfigProperties();
        this.caloriesSlider.setValue(configDTO.getCaloriesAmount());
        this.usernameTextField.setText(configDTO.getUserName());
        this.localizationTextField.setText(configDTO.getLocalization());
    }

}
