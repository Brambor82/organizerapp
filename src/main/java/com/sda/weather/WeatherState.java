package com.sda.weather;

public enum WeatherState {
    SUN, STORM, SNOW, RAIN, CLOUDS
}
